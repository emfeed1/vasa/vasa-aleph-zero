# Mat&Met
The sequence data were analysed using an in house developed Jupyter Notebooks (Kluyver et al, 2016) piping together different bioinformatics tools. Briefly, for both 16S and ITS data, R1 and R2 sequences were assembled using PEAR (Zhang et al., 2014) with default settings. Further quality checks were conducted using the QIIME pipeline (Caporaso et al., 2010) and short sequences were removed (< XXXbp for 16S and < XXXbp for ITS) . Reference based and de novo chimera detection, as well as clustering in OTUs were performed using VSEARCH (Rognes et al., 2016) and the adequate reference databases (SILVA’ representative set of sequences for 16S and SILVA’s ITS2 reference dynamic dataset for ITS). The identity thresholds were set at 94% for 16S data based on replicate sequencings of a bacterial mock community containing 40 bacterial species, and 97% for ITS data for which we did not have a mock community). For 16S data, representative sequences for each OTU were aligned using MAFFT (Kazutaka and all., 2013) and a 16S phylogenetic tree was constructed using FastTree (Price et al., 2009). Taxonomy was assigned using BLAST (Altschul et al., 1990) and the SILVA reference database v138 for 16S (Quast and all, 2013). For 18S, the taxonomy assignment was performed using BLAST (Altschul et al., 1990) and the PR2-primer reference database (Vaulot and all, 2021). For ITS, the taxonomy assignment was performed using BLAST (Altschul et al., 1990) and the UNITE reference database (Abarenkov and all., 2021).
Diversity metrics, that is, Faith’s Phylogenetic Diversity for 16S data only (Faith, 1992), richness (observed species, XXX) and evenness (Simpson’s reciprocal index, XXX), describing the structure of microbial communities were calculated based on rarefied OTU tables (XXX and XXX sequences per sample for 16S and ITS respectively). Bray-Curtis and UniFrac distance matrices (Lozupone and Knight, 2005) were also computed to detect global variations in the composition of microbial communities for ITS and 16S, respectively. 

# Citations

Zhang et al., 2014 :
Jiajie Zhang, Kassian Kobert, Tomáš Flouri, Alexandros Stamatakis; PEAR: a fast and accurate Illumina Paired-End reAd mergeR. Bioinformatics 2014; 30 (5): 614-620. doi: 10.1093/bioinformatics/btt593

Caporaso et al., 2010 :
QIIME allows analysis of high-throughput community sequencing data; J Gregory Caporaso, Justin Kuczynski, Jesse Stombaugh, Kyle Bittinger, Frederic D Bushman, Elizabeth K Costello, Noah Fierer, Antonio Gonzalez Pena, Julia K Goodrich, Jeffrey I Gordon, Gavin A Huttley, Scott T Kelley, Dan Knights, Jeremy E Koenig, Ruth E Ley, Catherine A Lozupone, Daniel McDonald, Brian D Muegge, Meg Pirrung, Jens Reeder, Joel R Sevinsky, Peter J Turnbaugh, William A Walters, Jeremy Widmann, Tanya Yatsunenko, Jesse Zaneveld and Rob Knight; Nature Methods, 2010; doi:10.1038/nmeth.f.303

Rognes et al., 2016
Rognes T, Flouri T, Nichols B, Quince C, Mahé F. (2016) VSEARCH: a versatile open source tool for metagenomics. PeerJ 4:e2584 https://doi.org/10.7717/peerj.2584

Kluyver et al, 2016 : 
Jupyter Notebooks – a publishing format for reproducible computational workflows; 2016; Thomas Kluyver, Benjamin Ragan-Kelley, Fernando Pérez, Brian Granger, Matthias Bussonnier, Jonathan Frederic, Kyle Kelley, Jessica Hamrick, Jason Grout, Sylvain Corlay, Paul Ivanov, Damián Avila, Safia Abdalla, Carol Willing, Jupyter Development Team; Pages: 87 - 90; DOI: 10.3233/978-1-61499-649-1-87

Kazutaka Katoh, Daron M. Standley, MAFFT Multiple Sequence Alignment Software Version 7: Improvements in Performance and Usability, Molecular Biology and Evolution, Volume 30, Issue 4, April 2013, Pages 772–780, https://doi.org/10.1093/molbev/mst010

Quast C, Pruesse E, Yilmaz P, Gerken J, Schweer T, Yarza P, Peplies J, Glöckner FO (2013) The SILVA ribosomal RNA gene database project: improved data processing and web-based tools. Opens external link in new windowNucl. Acids Res. 41 (D1): D590-D596.

Yilmaz P, Parfrey LW, Yarza P, Gerken J, Pruesse E, Quast C, Schweer T, Peplies J, Ludwig W, Glöckner FO (2014) The SILVA and "All-species Living Tree Project (LTP)" taxonomic frameworks. Opens external link in new windowNucl. Acids Res. 42:D643-D648

Abarenkov, Kessy; Zirk, Allan; Piirmann, Timo; Pöhönen, Raivo; Ivanov, Filipp; Nilsson, R. Henrik; Kõljalg, Urmas (2021): UNITE QIIME release for Fungi 2. Version 10.05.2021. UNITE Community. https://doi.org/10.15156/BIO/1264763

Vaulot, D., Mahé, F., Bass, D., & Geisen, S. (2021). pr2-primer : An 18S rRNA primer database for protists. Molecular Ecology Resources, in press. DOI: 10.1111/1755-0998.13465

Price MN, Dehal PS, Arkin AP. FastTree: computing large minimum evolution trees with profiles instead of a distance matrix. Mol Biol Evol. 2009;26(7):1641-1650. doi:10.1093/molbev/msp077

Altschul SF, Gish W, Miller W, Myers EW, Lipman DJ. Basic local alignment search tool. J Mol Biol. 1990 Oct 5;215(3):403-10. doi: 10.1016/S0022-2836(05)80360-2. PMID: 2231712.

Faith DP. 1992. Conservation evaluation and phylogenetic diversity. Biological Conservation 64, 1–10.

Lozupone C, Knight R. UniFrac: a new phylogenetic method for comparing microbial communities. Appl Environ Microbiol. 2005 Dec;71(12):8228-35. doi: 10.1128/AEM.71.12.8228-8235.2005. PMID: 16332807; PMCID: PMC1317376.

Lozupone, C., Lladser, M., Knights, D. et al. UniFrac: an effective distance metric for microbial community comparison. ISME J 5, 169–172 (2011). https://doi.org/10.1038/ismej.2010.133
